# 3) Given a positive number, print out all Fibonacci numbers smaller or equal to that number.
# For example, given the number 11 the program should print out: 1 1 2 3 5 8
# The next Fibonacci number would be 13 which is already larger than 11.

class Fibonacci:
    def __init__(self):
        self.memory = {1:0, 2:1, 3:1}
    def get(self, n):
        if n not in self.memory:
            self.memory[n] = self.get(n-1) + self.get(n-2)
        return self.memory[n]
    
position = int(input("Enter number: "))
f1 = Fibonacci()
a = f1.get(position)
j = position


def function_Fibo(x):
    if f1.memory[x] <= position:
        print(f1.memory)
    else:
        x = x - 1
        f1.get(x)
        function_Fibo(x)
 
function_Fibo(position)