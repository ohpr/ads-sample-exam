# 1) A palindrome is sequence that reads same backwards as forwards. “Anna”, for example is a palindrome. The task is to determine if a given string is a palindrome.

def palindromes(x): # I start by defining a function, which will take a variable "x", representing the word we want to test for being a palindrome.
    
    middle = len(x) // 2 # First, I set the exact middle of the word, since I will compare the first half with the second half of the word.
    for i in range(0, middle): # I create a for loop, in which I tell the algorithm, for all "i"s ranging from 0 (the first letter of the word) until "middle" (the last letter in the first half), do the following:
        if x[i] != x[-1-i]: # Here the algorithm will compare the first letter in the first half of the word with its counterpart position in the second half of the word.
                            # E.g., the first comparison is x[0] ! = x[-1 -0]; if this is true, then the algorithm will do the same for x[1] != x[-1-1 = -2], and so on.
            return False # In the case the words are indeed different, i.e. if the "unequality" above is met, then the loop will return a False and break.
    return True # When al "i"s are run succesfully (in the case the if statement is never met), then the loop returns a True, meaning that the word is indeed a palindrome.

palindromes() # The function can be tested here.

# Time complexity conclusion:

# The time complexity of this algorithm is of order O(n/2), since we are only looking at half the values.

# --------------------------- Visual Representation ---------------------------

# import time
# import matplotlib.pyplot as plt
    
# results = []

# a = str(111111111111111111)
# b = str(1111111111111111111111111111111)
# c = str(1111111111111111111111111111111111111111111111111)
# d = str(111111111111111111111111111111111111111111111111111111111111111111111111111111)

# dicta = {"names":[a, b, c, d]}
# # print(dicta["names"][1])

# for j in range(0, 4):
#     name_test = dicta["names"][j]
#     start = time.time()
#     palindromes(name_test)
#     end = time.time()
#     time_ = end - start
#     results.append(time_)
#     j = j + 1

# print(results)

# x_axis = [a, b, c, d]

# plt.plot(x_axis, results, marker = ".", label = "plo")
# plt.xlabel("length of names")