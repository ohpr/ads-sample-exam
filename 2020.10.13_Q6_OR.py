# 6) Determine given two strings if one is an anagram of the other.
# For example, the two strings “anagram” and “margana” are anagrams of each other.

word1 = input("Enter your word: ")
word2 = input("Enter your word: ")
anagram = word2[::-1]

if word1[::-1] == anagram:
    print("Yes, they are anagrams of each other")
else:
    print("No, they are not anagrams of each other")