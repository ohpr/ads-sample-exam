# 4) Given a sequence of numbers, determine if each element is larger than the previous element.

def sequence(list_): # As usual, I start by defining a function which will contain the input (in this case a list) we want to analyze.
    for i in range(1, len(list_)): # I set up a for loop, which will help us go through all numbers in the list.
        if list_[i-1] > list_[i]: # In combination with the for loop, I set the condition here that if a previous value (starting at 0, denoted by i - 1) ...
                                  # is larger than the following value, the loop will break as it will return False.
            return False # Returns False if the if condition is met.
        
    return True # If the condition above is never met, the function won't break and will continue until reaching the end of the list and comparing...
                # ... all previous numbers with their following numbers. In this case, the function returns a True.
        
list1 = [] # Enter list here to try the algorithm.

sequence(list1)

# Time complexity conclusion:

# The time complexity of the algorithm is of order O(n), as its time-complexity increases in line with the length of the list. 

# --------------------------- Visual Representation ---------------------------

# import time
# import matplotlib.pyplot as plt
    
# results = []


# j = 1000
# while j < 10**7:
#     lista = list(range(1, j))
#     start = time.time()
#     sequence(lista)
#     end = time.time()
#     time_ = end - start
#     results.append(time_)
#     j = j * 2

# print(results)

# x_axis = []
# i = 1000
# while i < 10**7:
#     x_axis.append(i)
#     i = i * 2
    
# plt.plot(x_axis, results, marker = ".", label = "plo")
# plt.xlabel("length of list")

