# 8) Calculate the frequency of items in a given sequence.


def frequencee(list_): # I define a function frequencee which will incorporate as variable a list.
                       # The algorithm will count the times each word appears inside the list.
    for i in range(0, len(list_)): # Then, I establish two for loops; the first one (using i), is used to fix the words inside the list, in order to compare them with the rest.
        results = [] # For each word, I create an empty list, where I save afterwards the times the word appears in the list.
        for j in range(0, len(list_)): # This is the second for loop, which I use to go through the words in the list and compare them with the word fixed with the first loop.
            if list_[i] == list_[j]: # This statement compares the word list_[i] (fixed with the first loop) with each "j" word in list_[j].
                results.append(1) # If it is the case, that both words are equal, then a "1" is stored in the results list for the word list_[i].
            j = j + 1 # I increase j to keep the loop moving and going to the next word. After j is done, the algorithm jumps up again and chooses the next i, and the same thing happens.
        print(list_[i] + ": " + str(len(results))) # After each "j" round, the algorithm prints the amount of appearances per word.

dicta = [] # Insert list to try the algorithm.
# dicta = ["hi", "I", "am", "Alexa", "I", "would", "just", "like", "to", "say", "hi"]
frequencee(dicta) 

# Time complexity conclusion:
    



