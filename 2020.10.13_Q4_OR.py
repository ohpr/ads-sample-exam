# 4) Given a sequence of numbers, determine if each element is larger than the previous element.

def function_sequence(list_):
    j = 0
    for i in range(1, len(list_)):
        if list_[j] < list_[i]:
            print("Yes, " + str(list_[i]) + " is larger than " + str(list_[j]))
        else:
            print("No, " + str(list_[i]) + " is not larger than " + str(list_[j]))
        j = j + 1

list1 = [1, 2, 3, 4, 7, 9, 3, 10, 22, 2, 1, 4, 3, 7, 3]

function_sequence(list1)