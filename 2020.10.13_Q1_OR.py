# 1) A palindrome is sequence that reads same backwards as forwards. “Anna”, for example is a palindrome. The task is to determine if a given string is a palindrome.

word = input("Enter your word: ")
palindrome = word[::-1]
if word == palindrome:
    print("Yes, this is a palindrome")
else:
    print("No, this is not a palindrome")