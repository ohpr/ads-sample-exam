# 3) Given a positive number, print out all Fibonacci numbers smaller or equal to that number.
# For example, given the number 11 the program should print out: 1 1 2 3 5 8 # The next Fibonacci number would be 13 which is already larger than 11.


def fibo(target): # First I define a formula which will carry the "target number" we can define - the formula will return the Fibonacci values smaller or equal to this number.
    list_ = [0, 1] # I start by creating a list, which will include the first two Fibonacci numbers of the sequence - this is necessary in order to get the sequence rolling.
    while True: # I tell the algorithm "while True" i.e. keep doing the following indifinitely unless yoou meet a break.
        new_number = list_[-1] + list_[-1-1] # Here, I calculate the value following the Fibonacci sequence, which is the addition of the previous two values.
                                             # By defining list[-1] and list[-1-1], I tell the algorithm to take the last two values inside the sequence stored in list_.
        list_.append(new_number) # After calculating the new number, I include it in the list storing the sequence.
        if new_number > target: # Having the new value stored, this line asks whether this new number is already larger than the target number that we enter to activate the function.
            break # If the if condition above is met - then we will break the loop, meaning that, we have indeed a number now that is larger than our input.
    return list_[0:-1] # Since the last "new_number" added to the list is larger than the "target", I return the sequence WITHOUT the last added value, which is evidently larger than our input.

# The function can be tried here:        
fibo()

# Time complexity conclusion:

# The time complexity of this algorithm is of order O(1), as the time-complexity depends solely on the input, which is one number - its size...
# doesn't really influence the time-complexity.

# --------------------------- Visual Representation ---------------------------

# import time
# import matplotlib.pyplot as plt
    
# results = []


# j = 1000
# while j < 10**7:
    
#     start = time.time()
#     fibo(j)
#     end = time.time()
#     time_ = end - start
#     results.append(time_)
#     j = j * 2

# print(results)

# x_axis = []
# i = 1000
# while i < 10**7:
#     x_axis.append(i)
#     i = i * 2
    
# plt.plot(x_axis, results, marker = ".", label = "plo")
# plt.xlabel("size of target")
